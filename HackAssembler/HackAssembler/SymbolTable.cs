﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace HackAssembler
{
    class SymbolTable
    {
        Hashtable Table = new Hashtable();
        int _nextAvailableAddress = 16;

        public SymbolTable()
        {
            InitialiseTable();
        }

        public void addEntry(string symbol)
        {
            Table.Add(symbol, (_nextAvailableAddress++).ToString());
        }

        public void addEntry(string symbol, string address)
        {
            Table.Add(symbol, address);
        }



        public bool contains(string symbol)
        {
            return Table.ContainsKey(symbol);
        }

        public int GetAddress(string symbol)
        {
            return Convert.ToInt32(Table[symbol]);
        }


        private void InitialiseTable()
        {
            Table.Add("SP", "0");
            Table.Add("LCL", "1");
            Table.Add("ARG", "2");
            Table.Add("THIS", "3");
            Table.Add("THAT", "4");
            
            //Init 16 registers
            for (int i = 0; i < 16; i++)
            {
                Table.Add("R" + i.ToString(), i.ToString());
            }

            Table.Add("SCREEN", "16384");
            Table.Add("KBD", "24576");

        }

    }
}
