﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace HackAssembler
{
    enum InstructionType {A_COMMAND, C_COMMAND, L_COMMAND}

    class Program
    {

        static string filePath = GetTargetFilePath(".asm");
        static string fileName, fileDirectory;

        static void Main(string[] args)
        {

            SplitFileFromPath(filePath, out fileName, out fileDirectory);

            Parser Parser = new Parser(filePath);
            Code CodeTranslator = new Code();
            SymbolTable Table = new SymbolTable();

            #region Testcode
            ////Test Code
            //while (Parser.hasMoreCommands)
            //{
            //    Parser.advance();

            #region ParserTest 
            //    //    Parser.PrintCommand();
            //    //    Console.WriteLine(Parser.commandType().ToString());
            //    //    if (Parser.commandType() == InstructionType.A_COMMAND)
            //    //        Console.WriteLine(Parser.symbol());
            //    //    else if (Parser.commandType() == InstructionType.L_COMMAND)
            //    //        Console.WriteLine(Parser.symbol());
            //    //    else if (Parser.commandType() == InstructionType.C_COMMAND)
            //    //    {
            //    //        Console.WriteLine(Parser.dest());
            //    //        Console.WriteLine(Parser.comp());
            //    //        Console.WriteLine(Parser.jump());
            //    //    }
            //    //    Console.WriteLine();
            #endregion

            #region CodeTest

            //    //Parser.PrintCommand();
            //    //if (Parser.commandType() == InstructionType.C_COMMAND)
            //    //{
            //    //    Console.WriteLine($"Dest: {CodeTranslater.dest(Parser.dest())}");
            //    //    Console.WriteLine($"Comp: {CodeTranslater.comp(Parser.comp())}");
            //    //    Console.WriteLine($"Jmp: {CodeTranslater.jmp(Parser.jump())}");
            //    //}

            //    //Console.WriteLine();

            #endregion

            //}
            #endregion

            PerformTranslation(Parser, CodeTranslator, Table);

            Console.WriteLine("Translation complete");

        }

        private static string GetTargetFilePath(string Extension)
        {
            Console.WriteLine("Enter the file name you would like to translate:");
            string filePath = Console.ReadLine();
            filePath += Extension;

            if (filePath.EndsWith(Extension) == false)
            {
                Console.WriteLine("The file is not a {0} file", Extension);
                System.Environment.Exit(0);
            }

            if (File.Exists(filePath) == false)
            {
                Console.WriteLine("The file cannot be found");
                System.Environment.Exit(0);
            }
            return filePath;
        }

        private static StreamReader OpenTargetFile(String filepath)
        {
            StreamReader assemblyFile = null;
            try
            {
                assemblyFile = new StreamReader(filepath);
            }
            catch (Exception e)
            {
                Console.WriteLine("The file could not be read");
                Console.WriteLine(e.Message);
                System.Environment.Exit(0);
            }
            return assemblyFile;
        }

        private static void SplitFileFromPath(String filepath, out string fileName, out string fileDirectory)
        {
            int startOfFileName = filepath.LastIndexOf("\\");
            if (startOfFileName == -1)
            {
                fileName = filepath;
                fileDirectory = "";
            }

            fileName = filepath.Remove(0, startOfFileName + 1);
            fileDirectory = filepath.Remove(startOfFileName + 1);
        }

        private static StreamWriter CreateHackFile(string fileName, string fileDirectory)
        {
            fileName = fileName.Replace(".asm", ".hack");
            return new StreamWriter(fileDirectory + fileName);
        }

        private static void PerformTranslation(Parser Parser, Code Translator, SymbolTable Table)
        {
            FirstPass(Parser, Table);
            SecondPass(Parser, Translator, Table);
        }

        private static void FirstPass(Parser Parser, SymbolTable Table)
        {
            int commandNumber = 0;

            while (Parser.hasMoreCommands)
            {
                Parser.advance();

                if (Parser.commandType() == InstructionType.L_COMMAND)
                {
                    Table.addEntry(Parser.symbol(), (commandNumber).ToString() );
                }
                else
                {
                    commandNumber++;
                }
            }
            Parser.ResetParser();
        }

        private static void SecondPass(Parser Parser, Code Translator, SymbolTable Table)
        {

            string binaryCommand = "";

            using (StreamWriter machineCode = CreateHackFile(fileName, fileDirectory))
            {
                while (Parser.hasMoreCommands)
                {
                    binaryCommand = "";
                    Parser.advance();
                   
                    if (Parser.commandType() != InstructionType.L_COMMAND)
                    {
                        binaryCommand = TranslateCommand(Parser, Translator, Table);
                        machineCode.WriteLine(binaryCommand);
                    }
 
                }
            }
        }

        private static string TranslateCommand(Parser Parser, Code Translator, SymbolTable Table)
        {
            string binaryCommand = "";

            switch (Parser.commandType())
            {
                case InstructionType.A_COMMAND:
                    binaryCommand = TranslateACommand(Parser, Translator, Table);
                break;

                case InstructionType.C_COMMAND:
                    binaryCommand = TranslateCCommand(Parser, Translator);
                break;
            }

            return binaryCommand;

        }

        private static string TranslateACommand (Parser Parser, Code Translator, SymbolTable Table)
        {
            string binaryCommand = "";

            string symbol = Parser.symbol();

            if (BeginsWithDigit(symbol))
                binaryCommand += Translator.SymbolToCode(symbol);
            else
            {
                if (Table.contains(symbol))
                    binaryCommand = Table.GetAddress(symbol).ToString();
                else
                {
                    Table.addEntry(symbol);
                    binaryCommand = Table.GetAddress(symbol).ToString();
                }

                binaryCommand = Translator.SymbolToCode(binaryCommand);
            }

            return binaryCommand.Insert(0, Translator.GetACommandPrefix());
        }

        private static bool BeginsWithDigit(string testString)
        {
            string digits = "0123456789";
            if (digits.Contains(testString[0]))
                return true;
            else
                return false;
        }

        private static string TranslateCCommand(Parser Parser, Code Translator)
        {
            string binaryCommand = Translator.GetCCommandPrefix();
            binaryCommand += Translator.comp(Parser.comp());
            binaryCommand += Translator.dest(Parser.dest());
            binaryCommand += Translator.jump(Parser.jump());
            return binaryCommand;
        }
    }
}


