﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace HackAssembler
{
    
    class Code
    {

        private Hashtable destTable = new Hashtable(8);     // Contains string representation of 3bit destination code
        private Hashtable compTable = new Hashtable(28);    // Contains string representation of 6bit comp code prefaced by 1bit a bit
        private Hashtable jmpTable = new Hashtable(8);     // Contains string representation of 3bit jmp code

        public Code ()
        {
            InitDestTable();
            InitCompTable();
            InitJmpTable();
        }

        #region PublicmMthods
        public string dest(string mnemonic)
        {
            if (destTable.ContainsKey(mnemonic))
                return destTable[mnemonic].ToString();
            return "";
        }

        public string comp(string mnemonic)
        {
            if (compTable.ContainsKey(mnemonic))
                return compTable[mnemonic].ToString();
            return "";
        }

        public string jump(string mnemonic)
        {
            if (jmpTable.ContainsKey(mnemonic))
                return jmpTable[mnemonic].ToString();
            return "";
        }

        public string GetACommandPrefix()
        {
            return "0";
        }

        public string GetCCommandPrefix()
        {
            return "111";
        }

        public string SymbolToCode(int symbol)
        {
            string binaryAddress = Convert.ToString(symbol, 2);

            return PadAddress(binaryAddress);
        }
        public string SymbolToCode(string symbol)
        {
            //convert decimal address to binary
            int address = Convert.ToInt32(symbol); 
            string binaryAddress = Convert.ToString(address, 2);
                       
            return PadAddress(binaryAddress);

        }

        #endregion


        #region PrivateMethods

        private void InitDestTable()
        {
            destTable.Add("", "000");
            destTable.Add("M", "001");
            destTable.Add("D", "010");
            destTable.Add("MD", "011");
            destTable.Add("A", "100");
            destTable.Add("AM", "101");
            destTable.Add("AD", "110");
            destTable.Add("AMD", "111");
        }

        private void InitCompTable()
        {
            //a=0
            compTable.Add("0", "0101010");
            compTable.Add("1", "0111111");
            compTable.Add("-1", "0111010");
            compTable.Add("D", "0001100");

            compTable.Add("A", "0110000");
            compTable.Add("!D", "0011001");
            compTable.Add("!A", "0110001");
            compTable.Add("-D", "0001111");

            compTable.Add("-A", "0110011");
            compTable.Add("D+1", "0011111");
            compTable.Add("A+1", "0110111");
            compTable.Add("D-1", "0001110");

            compTable.Add("A-1", "0110010");
            compTable.Add("D+A", "0000010");
            compTable.Add("D-A", "0010011");
            compTable.Add("A-D", "0000111");

            compTable.Add("D&A", "0000000");
            compTable.Add("D|A", "0010101");

            //a=1
            compTable.Add("M", "1110000");
            compTable.Add("!M", "1110001");
            compTable.Add("-M", "1110011");
            compTable.Add("M+1", "1110111");

            compTable.Add("M-1", "1110010");
            compTable.Add("D+M", "1000010");
            compTable.Add("D-M", "1010011");
            compTable.Add("M-D", "1000111");

            compTable.Add("D&M", "1000000");
            compTable.Add("D|M", "1010101");
        }

        private void InitJmpTable()
        {
            jmpTable.Add("", "000");
            jmpTable.Add("JGT", "001");
            jmpTable.Add("JEQ", "010");
            jmpTable.Add("JGE", "011");
            jmpTable.Add("JLT", "100");
            jmpTable.Add("JNE", "101");
            jmpTable.Add("JLE", "110");
            jmpTable.Add("JMP", "111");
        }

        private string PadAddress(string address)
        {
            //Pad with 0s to correct length
            int DigitsToPad = 15 - address.Length;
            for (int i = 0; i < DigitsToPad; i++)
            {
                address = address.Insert(0, "0");
            }

            return address;
        }

        #endregion

    }
}
