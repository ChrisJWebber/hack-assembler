﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace HackAssembler
{
    class Parser : IDisposable
    {
        bool disposed = false;

        private StreamReader assemblyCode;
        string currentCommand = "";
        public Parser (string filePath)
        {
            this.assemblyCode = new StreamReader(filePath);
        }

        #region PublicMethods

        public bool hasMoreCommands
        {
            get
            {
                return !assemblyCode.EndOfStream;
            }
        }

        public void advance()
        {
            do
            {
                // advance to next command and remove all white space
                currentCommand = assemblyCode.ReadLine();
                currentCommand = currentCommand.Replace(" ", "");

                //Remove escape sequence tabs
                currentCommand = currentCommand.Replace("\t", "");

                //Remove comments
                int commentIndex = currentCommand.IndexOf("/");
                if (commentIndex != -1)
                    currentCommand = currentCommand.Remove(commentIndex);
            } while (currentCommand.Equals(""));
        }

        public InstructionType commandType()
        {
            if (currentCommand.StartsWith("@"))
                return InstructionType.A_COMMAND;

            if (currentCommand.StartsWith("("))
                return InstructionType.L_COMMAND;

            return InstructionType.C_COMMAND;


        }

        public string symbol()
        {
            if (commandType() == InstructionType.A_COMMAND)
            {             
                // remove leading "@" and return
                return currentCommand.Remove(0,1);
            }

            else if (commandType() == InstructionType.L_COMMAND)
            {
                // Remove leading "(" and trailing ")"
                string temp = currentCommand.Remove(0, 1);
                return temp.Remove(temp.Length - 1);
            }
            else
            {
                return "";
            }

        }

        public string dest()
        {
            int endOfDest = currentCommand.IndexOf("=");
            if (endOfDest == -1)
                return "";
            return currentCommand.Remove(endOfDest);
        }

        public string comp()
        {
            string temp = currentCommand;
            int endOfComp = temp.IndexOf(";");
            int endOfDest = temp.IndexOf("=");

            if (endOfComp != -1)
                temp = temp.Remove(endOfComp);

            if (endOfDest != -1)
                temp = temp.Remove(0, endOfDest + 1);

            if ((endOfComp == -1) && (endOfDest == -1))
                return "";
            return temp;         
        }

        public string jump()
        {
            int endOfComp = currentCommand.IndexOf(";");
            if (endOfComp == -1)
                return "";
            return currentCommand.Remove(0,endOfComp + 1);
        }

        public void ResetParser()
        {
            assemblyCode.BaseStream.Position = 0;
            assemblyCode.DiscardBufferedData();
        }

        public void PrintCommand ()
        {
            //Used in test code
            Console.WriteLine("Current command: {0}", currentCommand);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        #region PrivateMethods

        private bool CheckValidCCommand (string command)
        {
            throw new NotImplementedException();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
            {
                return;
            }

            if (disposing)
            {
                assemblyCode.Dispose();
            }

            disposed = true;
        }

        #endregion 

        



    }
}
